#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/ValueSymbolTable.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/IRReader/IRReader.h"
#include <iostream>
#include <map>
#include <set>
#include <fstream>

using namespace llvm;
using namespace std;

struct DefUseGraph
{
    map<Value *, set<Value *>> defUseMap;

    void addDefUse(Value *def, Value *use)
    {
        defUseMap[def].insert(use);
    }
};

struct CallGraph
{
    map<Function *, set<Function *>> callMap;

    void addCall(Function *caller, Function *callee)
    {
        callMap[caller].insert(callee);
    }
};

void generateDefUseDotFile(DefUseGraph &graph, const string &filename)
{
    ofstream DotFile(filename);
    if (!DotFile.is_open()) {
        errs() << "Unable to open output file.\n";
        return;
    }

    DotFile << "digraph DefUseGraph {\n";
    DotFile << "  rankdir=\"BT\";\n";
    for (auto &entry : graph.defUseMap) {
        Value *def = entry.first;
        set<Value *> uses = entry.second;

        string defStr;
        raw_string_ostream defStream(defStr);
        def->print(defStream);

        DotFile << "  \"" << defStr << "\" [label=\"" << defStr << "\"];\n";

        for (Value *use : uses) {
            string useStr;
            raw_string_ostream useStream(useStr);
            use->print(useStream);
            DotFile << "  \"" << useStr << "\" -> \"" << defStr << "\";\n";
        }
    }

    DotFile << "}\n";
    DotFile.close();
}

void generateCallGraphDotFile(CallGraph &graph, const string &filename)
{
    ofstream DotFile(filename);
    if (!DotFile.is_open()) {
        errs() << "Unable to open output file.\n";
        return;
    }

    DotFile << "digraph CallGraph {\n";
    DotFile << "  rankdir=\"TB\";\n";

    for (auto &entry : graph.callMap) {
        Function *caller = entry.first;
        set<Function *> callees = entry.second;

        DotFile << "  \"" << caller->getName().str() << "\" [label=\"" << caller->getName().str() << "\"];\n";

        for (Function *callee : callees) {
            DotFile << "  \"" << caller->getName().str() << "\" -> \"" << callee->getName().str() << "\";\n";
        }
    }

    DotFile << "}\n";
    DotFile.close();
}

int main(int argc, char **argv) {
    llvm::cl::opt<std::string> FileName("input", llvm::cl::desc("Input LLVM IR file"), llvm::cl::Positional, llvm::cl::Required);

    llvm::cl::ParseCommandLineOptions(argc, argv, "LLVM IR Analysis Tool\n");

    LLVMContext Context;
    SMDiagnostic Err;
    unique_ptr<Module> Mod = parseIRFile(FileName.c_str(), Err, Context);
    if (!Mod) {
        Err.print(nullptr, errs());
        return 1;
    }

    DefUseGraph defUseGraph;
    CallGraph callGraph;

    for (auto &Func : *Mod) {
        for (auto &BB : Func) {
            for (auto &Inst : BB) {
                if (auto *DefInst = dyn_cast<Instruction>(&Inst)) {
                    for (Use &U : DefInst->uses()) {
                        Instruction *UseInst = cast<Instruction>(U.getUser());
                        defUseGraph.addDefUse(DefInst, UseInst);
                    }
                }

                if (auto *CI = dyn_cast<CallInst>(&Inst)) {
                    if (Function *Caller = CI->getFunction()) {
                        Function *Callee = CI->getCalledFunction();
                        if (Callee) {
                            callGraph.addCall(Caller, Callee);
                        }
                    }
                }
            }
        }
    }

    generateDefUseDotFile(defUseGraph, "def_use_graph.dot");
    generateCallGraphDotFile(callGraph, "call_graph.dot");

    cout << "Def-Use graph DOT file generated: def_use_graph.dot\n";
    cout << "Call graph DOT file generated: call_graph.dot\n";

    return 0;
}
