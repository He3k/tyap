#! /bin/bash

# Генерация LLVM IR файла для программы sum.c с отключением оптимизаций
clang-12 -S -emit-llvm sum.c
# Компиляция и линковка main.cpp с использованием llvm-config для получения флагов компиляции, линковки и библиотек
clang++ -o gb `llvm-config --cxxflags --ldflags --libs all` main.cpp
# Выполнение программы gb с аргументом sum.ll (LLVM IR файл sum.c)
./gb sum.ll
# Создание изображения (формата PNG) из файла DOT-графа def_use_graph.dot
dot -Tpng def_use_graph.dot -o sumo0.png

# Генерация LLVM IR файла для программы sum.c с включением оптимизаций уровня O3
clang-12 -S -emit-llvm -O3 sum.c
# Компиляция и линковка main.cpp с использованием llvm-config для получения флагов компиляции, линковки и библиотек
clang++ -o gb `llvm-config --cxxflags --ldflags --libs all` main.cpp
# Выполнение программы gb с аргументом sum.ll (LLVM IR файл sum.c) после оптимизаций O3
./gb sum.ll
# Создание изображения (формата PNG) из файла DOT-графа def_use_graph.dot после оптимизаций O3
dot -Tpng def_use_graph.dot -o sumo3.png

# Генерация LLVM IR файла для программы fac.c
clang-12 -S -emit-llvm fac.c
# Компиляция и линковка main.cpp с использованием llvm-config для получения флагов компиляции, линковки и библиотек
clang++ -o gb `llvm-config --cxxflags --ldflags --libs all` main.cpp
# Выполнение программы gb с аргументом fac.ll (LLVM IR файл fac.c)
./gb fac.ll
# Создание изображения (формата PNG) из файла DOT-графа call_graph.dot
dot -Tpng call_graph.dot -o faco0.png
