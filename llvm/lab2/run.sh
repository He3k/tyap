#!/bin/bash

clang-12 -S -O3 -emit-llvm -mllvm -disable-llvm-optzns source.c
opt-12 -O3 -S -sroa -tailcallelim source.ll -o source_optimized.ll --print-before=tailcallelim > before_tailcallelim.txt 2>&1
opt-12 -O3 -S -sroa -tailcallelim source.ll -o source_optimized.ll --print-after=tailcallelim > after_tailcallelim.txt 2>&1
opt-12 -O3 -S -sroa -tailcallelim source.ll -o source_optimized.ll --print-before=sroa > before_sroa.txt 2>&1
opt-12 -O3 -S -sroa -tailcallelim source.ll -o source_optimized.ll --print-after=sroa > after_sroa.txt 2>&1


