#! /bin/bash

sudo apt update && sudo apt upgrade -y
sudo apt install openjdk-11-* -y
sudo apt install gcc g++ cmake make -y
sudo apt install llvm-12 -y
sudo apt install graphviz -y
sudo apt install clang-12 -y
